from google.cloud import vision
from utils import bq_client
from utils import vision_client


def annotate_image(url):
    image_uri = url

    image = vision.Image()
    image.source.image_uri = image_uri

    response_label = vision_client.label_detection(image=image)
    response_text = vision_client.text_detection(image=image)
    response_landmark = vision_client.landmark_detection(image=image)
    response_faces = vision_client.face_detection(image=image)

    likelihood_name = ('UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE',
                       'LIKELY', 'VERY_LIKELY')

    labels = [(label.description, " {:.{}f}".format(label.score * 100, 0)) for label in
              response_label.label_annotations]

    texts = [text.description for text in response_text.text_annotations]

    landmarks = [landmark.description for landmark in response_landmark.landmark_annotations]

    faces = [('anger: {}'.format(likelihood_name[face.anger_likelihood]),
              'joy: {}'.format(likelihood_name[face.joy_likelihood]),
              'surprise: {}'.format(likelihood_name[face.surprise_likelihood]),
              'sorrow: {}'.format(likelihood_name[face.sorrow_likelihood]),
              'headwear: {}'.format(likelihood_name[face.headwear_likelihood]),
              'anger: {}'.format(likelihood_name[face.anger_likelihood])) for face in response_faces.face_annotations]

    return [labels, texts, landmarks, faces]


def annotate_ads_from_bq(project_name, dataset_name, table_name):
    query_template = """
        SELECT 
            ad_id,
            ad_group_id,
            campaign_id,
            image_url,
            campaign_start_date,
            campaign_end_date
        FROM `{0}.{1}.{2}`
        WHERE 
            image_url != ''
    """
    query = query_template.format(project_name, dataset_name, table_name)

    query_job = bq_client.query(query)  # Make an API request.

    annotated_ads = []

    print('Processing {} data rows from BQ ads table'.format(len(list(query_job))))
    for row in query_job:
        # Row values can be accessed by field name or index
        #print(row[0], row[1], row[2], row[3], row[4], row[5])
        # print(annotate_image(row[2])[0])
        annotated_ad = annotate_image(row[3])
        annotated_ads.append({
            'ad_id': row[0],
            'ad_group_id': row[1],
            'campaign_id': row[2],
            'image_url': row[3],
            'campaign_start_date': str(row[4]),
            'campaign_end_date': str(row[5]),
            'labels': ' '.join([str(elem) for elem in annotated_ad[0]]),
            'texts': ' '.join([str(elem) for elem in annotated_ad[1]]),
            'landmarks': ' '.join([str(elem) for elem in annotated_ad[2]]),
            'faces': ' '.join([str(elem) for elem in annotated_ad[3]]),
        })
    print('Annotated {} ad images'.format(len(annotated_ads)))
    return annotated_ads
