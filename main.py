# https://developers.google.com/identity/protocols/oauth2/web-server#example

import os
import flask
import google_auth_oauthlib.flow
import yaml
from ga_helper import *
from bq_helper import *
from vision_helper import *

# This variable specifies the name of a file that contains the OAuth 2.0
# information for this application, including its client_id and client_secret.
CLIENT_SECRETS_FILE = "client_secret.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPES = ['https://www.googleapis.com/auth/adwords']

app = flask.Flask(__name__)
# Note: A secret key is included in the sample so that it works.
# If you use this code in your application, replace this with a truly secret
# key. See https://flask.palletsprojects.com/quickstart/#sessions.
app.secret_key = 'psdbaodifnboadifnbeirnbeqirnbqer'


@app.route('/')
@app.route('/<info>')
def index(info=''):
    return print_index_table(info)


# https://developers.google.com/identity/protocols/oauth2/web-server#example

@app.route('/authorize')
def authorize():
    # Create flow instance to manage the OAuth 2.0 Authorization Grant Flow steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES)

    # The URI created here must exactly match one of the authorized redirect URIs
    # for the OAuth 2.0 client, which you configured in the API Console. If this
    # value doesn't match an authorized URI, you will get a 'redirect_uri_mismatch'
    # error.
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True, _scheme='https')

    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        access_type='offline',
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')

    # Store the state so the callback can verify the auth server response.
    flask.session['state'] = state

    return flask.redirect(authorization_url)


@app.route('/oauth2callback')
def oauth2callback():
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    state = flask.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES, state=state)
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True, _scheme='https')

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = flask.request.url.replace('http://', 'https://', 1)

    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    credentials = flow.credentials
    flask.session['credentials'] = credentials_to_dict(credentials)

    with open('/tmp/' + 'google-ads.yaml') as file:
        google_ads_conf = yaml.safe_load(file)

    google_ads_conf['client_id'] = credentials.client_id
    google_ads_conf['client_secret'] = credentials.client_secret
    google_ads_conf['refresh_token'] = credentials.refresh_token

    with open('/tmp/' + 'google-ads.yaml', 'w') as file:
        yaml.dump(google_ads_conf, file)

    return flask.redirect(location=flask.url_for("index", info='Credentials successfully retrived' +
                                                               'and saved in google-ads.yaml file .'))


@app.route('/get_params', methods=['GET', 'POST'])
def form():
    if flask.request.method == 'GET':
        return flask.render_template('form.html')
    if flask.request.method == 'POST':
        request_json = flask.request.get_json(silent=True)
        request_args = flask.request.args
        request_form = flask.request.form

        function_params = ('profile_id', 'developer_token', 'start_date', 'end_date', 'input_project_name',
                           'input_dataset_name', 'input_table_name')

        if request_form and all(elem in function_params for elem in request_form):
            profile_id: int = int(request_form['profile_id'])
            developer_token: str = request_form['developer_token']
            start_date: str = request_form['start_date']
            end_date: str = request_form['end_date']
            input_project_name: str = request_form['input_project_name']
            input_dataset_name: str = request_form['input_dataset_name']
            input_table_name: str = request_form['input_table_name']
        elif request_json is not None and all(elem in function_params for elem in request_json):
            profile_id: int = int(request_json['profile_id'])
            developer_token: str = request_json['developer_token']
            start_date: str = request_json['start_date']
            end_date: str = request_json['end_date']
            input_project_name: str = request_json['input_project_name']
            input_dataset_name: str = request_json['input_dataset_name']
            input_table_name: str = request_json['input_table_name']
        elif len(request_args) > 0 and all(elem in function_params for elem in request_args):
            profile_id: int = int(request_args['profile_id'])
            developer_token: str = request_args['developer_token']
            start_date: str = request_args['start_date']
            end_date: str = request_args['end_date']
            input_project_name: str = request_args['input_project_name']
            input_dataset_name: str = request_args['input_dataset_name']
            input_table_name: str = request_args['input_table_name']
        else:
            return 'Error getting required parameters', 500

        with open('/tmp/' + 'google-ads.yaml') as file:
            google_ads_conf = yaml.safe_load(file)

        google_ads_conf['login_customer_id'] = profile_id
        google_ads_conf['developer_token'] = developer_token

        with open('/tmp/' + 'google-ads.yaml', 'w') as file:
            yaml.dump(google_ads_conf, file)

        annotated_data = []

        # Get ads data from Google ADS
        ads = fetch_ga_ads(login_customer_id=profile_id,
                           start_date=start_date,
                           end_date=end_date)

        # If we got some data, save it into BQ table for processing.
        if ads and len(ads) > 0:
            print('Saving collected ads data into BQ.')
            write_batch_to_json(ads, "ads.json")

            write_to_bq(dataset_name=input_dataset_name,  # BQ Dataset Name
                        table_name=input_table_name,  # BQ Table Name
                        table_file_name="ads.json",  # json file name
                        job_message='with ads images to be annotated')  # job message

            # Once new data is saved lets annotate it.
            annotated_data = annotate_ads_from_bq(input_project_name, input_dataset_name, input_table_name)
        else:
            print('No ads data to be processed')

        # Save annotated data into BQ for analysis.
        if annotated_data and len(annotated_data) > 0:
            # We create new line delimited JSON file for easy loading into BQ
            write_batch_to_json(annotated_data, "ads_annotaated.json")

            write_to_bq(dataset_name=input_dataset_name,  # BQ Dataset Name
                        table_name=input_table_name,  # BQ Table Name
                        table_file_name="ads_annotaated.json",  # json file name
                        job_message='with annotated ads images')  # job message

        else:
            print('No annotated data to be saved.')

        return flask.redirect(location=flask.url_for("index", info='Ads data successfully annotated!!!'))


def credentials_to_dict(credentials):
    return {'token': credentials.token,
            'refresh_token': credentials.refresh_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes}


def print_index_table(info):
    return (str(info) +
            '<table>' +
            '<tr><td><a href="' + flask.url_for("authorize") +
            '">1.Run auth flow </a></td>' +
            '<td> Go directly to the authorization flow. ' +
            '    You will be prompted to authorize ' +
            '    the application, this generate refresh token for Google Ads API.</td></tr>' +
            '<tr><td><a href="' + flask.url_for("form") +
            '">2.Enter required parameters and run ads annotation.</a></td>' +
            '<td>Collect Google Ads image assets links and store them in BigQuery. ' +
            '    Cloud Vision API will annotate this ads data and append annotation ' +
            '    data to given table.</td></tr>' +
            '</td></tr></table>')


if __name__ == '__main__':
    # When running locally, disable OAuthlib's HTTPs verification.
    # ACTION ITEM for developers:
    #     When running in production *do not* leave this option enabled.
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    # Specify a hostname and port that are set as a valid redirect URI
    # for your API project in the Google API Console.
    app.run(host='localhost', port=8080, ssl_context='adhoc', debug=False)
