from utils import ga_client


def fetch_ga_ads(login_customer_id, start_date=None, end_date=None):
    """Fetches creatives using the Google Ads API.

  Args:
    login_customer_id: Google Ads Login customer ID configuration
    start_date: filter on last changelog timestamp
    end_date: filter on last changelog timestamp

  Returns:
    list of dicts, each with keys Ad_ID, Ad_Group_ID, Campaign_ID, Campaign_Start_date, Campaign_End_Date,
      Image_Full_URL, YT_videos_URLs
  """

    print('Fetching ads data from Google Ads for given time range and account id.')

    query_template = """
      SELECT
          ad_group_ad.ad.id, 
          ad_group.id, 
          campaign.id,
          campaign.labels,
          campaign.start_date,
          campaign.end_date,
          ad_group_ad.ad.image_ad.image_url,
          ad_group_ad.ad.app_ad.youtube_videos
      FROM ad_group_ad
      WHERE
          campaign.start_date > '{0}'
          AND campaign.end_date < '{1}'"""

    query = query_template.format(start_date, end_date)

    # Issues a search request using streaming.
    stream = ga_client.search_stream(customer_id=str(login_customer_id), query=query)

    out_ads = []  # final output collection
    row_num = 0

    for batch in stream:
        # print(batch.results)
        for row in batch.results:
            if row.ad_group_ad.ad.image_ad.image_url or len(row.ad_group_ad.ad.app_ad.youtube_videos) > 0:
                out_ads.append({
                    'ad_id': row.ad_group_ad.ad.id,
                    'ad_group_id': row.ad_group.id,
                    'campaign_id': row.campaign.id,
                    'campaign_start_date': row.campaign.start_date,
                    'campaign_end_date': row.campaign.end_date,
                    'image_url': row.ad_group_ad.ad.image_ad.image_url,
                    'yt_videos': str(row.ad_group_ad.ad.app_ad.youtube_videos),
                })
                row_num += 1
    # print(out_ads)
    print('...found {} creatives with suitable assets'.format(len(out_ads)))
    return out_ads
