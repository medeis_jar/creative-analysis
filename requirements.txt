# Function dependencies, for example:
# package>=version
google-ads
googleads~=30.0.0
google-cloud-bigquery
google-cloud-vision
ndjson~=0.3.1
Flask~=2.0.2
requests~=2.27.1
pyyaml
gunicorn
flask-talisman
pyopenssl