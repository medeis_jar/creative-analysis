# Creative-Analysis



## Getting started

This is sample implementation of Google Ads creatives Image annotation using [Vision AI](https://cloud.google.com/vision/docs/features-list) without the need of donloading the images. Creative image annotations data will be joined with Google Ads Data Transfer report in BigQuery allowing for brother analysys. 

Process flow:

1. Connect to Google Ads.
2. Based on filtering crtitera pull the report in Google Ads and get the list of creatives (id, camaign id, image_url).
3. Process every crreative image in Vision API and get annotations.
4. Create json file with feed of all data and import in in BigQuery dataset (can be the same as you hold Google Ads Data Transfer report).
5. Done.

