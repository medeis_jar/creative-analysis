import yaml
from google.ads.googleads.client import GoogleAdsClient
from google.auth import exceptions
from google.cloud import bigquery
from google.cloud import vision
from google.oauth2 import service_account


def get_credentials():
    # https://cloud.google.com/docs/authentication/production#automatically
    credentials = service_account.Credentials.from_service_account_file(
        'credentials.json', scopes=["https://www.googleapis.com/auth/cloud-platform"])
    print("credentials" + str(credentials))
    return credentials


def init_ga():
    # https://developers.google.com/google-ads/api/docs/client-libs/python/configuration#configuration_using_a_dict

    # Initial google-ads.yaml creation
    google_ads_dict = {'client_id': '619225017588-4o89atg8notlkkpen6qbnprv47bh89jt.apps.googleusercontent.com',
                       'client_secret': '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',
                       'refresh_token': '1//0cVdlH2J2va31CgYIARAAGAwSNwF'
                                        '-L9IrR6zeLHF3_8sANMHOozVYD3eWpIKcCRx7KzBfk40_okaQFXahrqp8VaQRyN4QgW151W4',
                       'login_customer_id': 8696015450,
                       'developer_token': 'NkOMSAVepPGG8QXrlpmkCw',
                       'use_proto_plus': True}

    with open('/tmp/' + 'google-ads.yaml', 'w') as file:
        yaml.dump(google_ads_dict, file)

    try:
        googleads_client = GoogleAdsClient.load_from_storage('google-ads.yaml', version="v9")
    except:
        print('Error getting local google-ads.yaml. Fail-over to generated google-ads.yaml in /tmp/')
        googleads_client = GoogleAdsClient.load_from_storage('/tmp/' + 'google-ads.yaml', version="v9")

    ga_service = googleads_client.get_service("GoogleAdsService")

    return ga_service


def init_bq():
    try:
        bq_client = bigquery.Client()
    except exceptions.DefaultCredentialsError:
        print('Error getting default Service Account credentials. Fail-over to credentials.json')
        credentials = get_credentials()
        bq_client = bigquery.Client(credentials=credentials, project=credentials.project_id)

    return bq_client


def init_vision():
    try:
        vision_service = vision.ImageAnnotatorClient()
    except exceptions.DefaultCredentialsError:
        print('Error getting default Service Account credentials. Fail-over to credentials.json')
        credentials = get_credentials()
        vision_service = vision.ImageAnnotatorClient(credentials=credentials)
    return vision_service


bq_client = init_bq()
ga_client = init_ga()
vision_client = init_vision()
