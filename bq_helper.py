from utils import bq_client
from google.cloud import bigquery
import ndjson


def write_batch_to_json(ads_data, batch_file_name):
    with open('/tmp/' + batch_file_name, 'w') as f:
        ndjson.dump(ads_data, f)


def write_to_bq(dataset_name, table_name, table_file_name, job_message):
    try:
        dataset_ref = bq_client.dataset(dataset_name)
        table_ref = dataset_ref.table(table_name)
        job_config = bigquery.LoadJobConfig()
        job_config.write_disposition = 'WRITE_TRUNCATE'
        job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
        job_config.autodetect = True
        with open('/tmp/' + table_file_name, "rb") as source_file:
            job = bq_client.load_table_from_file(source_file, table_ref, job_config=job_config)
            job.result()
        print('Loaded {} table {} into BQ'.format(table_name, job_message))
    except Exception as e:
        print('ERROR - {}: {}'.format(table_name, e))
